package uy.tchaqui.dev.test;

import static org.junit.Assert.*;

import org.junit.Test;

public class UtilTest {

	@Test
	public void testSum() {
		Util util = new Util();
		int expected =8;
		int actual = util.sum(6,2);
		
		assertEquals(expected, actual);
	}

}
